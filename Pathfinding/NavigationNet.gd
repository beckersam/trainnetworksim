class_name NaviagationNet extends Node2D
# TODO: Update all of this once I have a stable system for describing a net

var _stations: Dictionary
var id_to_station: Dictionary
var astar := AStar2D.new()


func register_station(station) -> void:
	_stations[station] = len(_stations)
	id_to_station[_stations[station]] = station
	# Add station to A* object. 
	# Its id is the index in the internally kept list of stations
	# Position is global position
	# Weight is number of outbound connections
	astar.add_point(_stations[station], station.global_position, len(station.targets))


func connect_stations() -> void:
	for i in _stations:
		var station = i
		for target in station.targets:
			astar.connect_points(_stations[i], _stations[target], false)


func try_find_path(start, end) -> Array:
	var id_path := astar.get_id_path(_stations[start], _stations[end])
	
	var path: Array = []
	
	for id in id_path:
		path.append(id_to_station[id])
		
	return path
