## A connector can be: 
## - The start / end of a platform at a station
## - A transfer between track segments (for example to change speed limits)
## - A line switch
class_name Connector extends Node2D

class ConnectionSetupState extends RefCounted:
	var conn: Connection
	var delete: bool
	var ok: bool

@export var data: ConnectorData

@export_group("Visuals")
@export_subgroup("Highlighter")
@export var highlight_color: Color
@export var selected_color: Color

@export_subgroup("Main Dot")
@export var invalid_setup_color: Color
@export var valid_setup_color: Color


@onready var main_dot: MeshInstance2D = $MeshInstance2D
@onready var highlighter: Sprite2D = $HighlighterRing

var connection_preload := preload("res://StaticElements/Connection/Connection.tscn")
var warning_scene := preload("res://Generic/Warning.tscn")

var default_color_dot: Color
var default_color_highlighter: Color
var selected := false
var outbound_connections: Array[Connection] = []
var inbound_connections: Array[Connection] = []

var _warning: Warning

func _ready() -> void:
	default_color_highlighter = highlighter.modulate
	default_color_dot = main_dot.modulate
	# TODO: Add deserialisation from save file and coloring on start
	_update_warning()


## Called from outside to request a connection to this node
## Returns ConnectionSetupState object
##
## Intended to be what the 2nd selected connector calls on the 1st selected connector
func request_connection(from: Connector) -> ConnectionSetupState:
	var c_state := ConnectionSetupState.new()
	# If already at max outbound connections, deny request
	if len(outbound_connections) > data.lines_in:
		c_state.ok = false
		c_state.conn = null
		c_state.delete = false
		Globals.first_selected = null
		highlighter.visible = false
		return c_state
	
	# If the connection already exists, delete it
	for i in len(outbound_connections):
		var c := outbound_connections[i] as Connection
		if c.is_connected_to(from):
			c_state.ok = true
			c_state.conn = null
			c_state.delete = true
			c.queue_free()
			selected = false
			outbound_connections.remove_at(i)
			Globals.first_selected = null
			highlighter.visible = false
			_update_warning()
			return c_state
	
	# Setup new connection
	Globals.first_selected = null
	highlighter.visible = false
	var conn: Connection = connection_preload.instantiate()
	conn.call_deferred("setup", from, self)
	add_child(conn)
	outbound_connections.append(conn)
	c_state.conn = conn
	c_state.ok = true
	c_state.delete = false
	selected = false
	_update_warning()
	return c_state


func _on_mouse_entered() -> void:
	if not selected:
		highlighter.modulate = highlight_color
		highlighter.visible = true


func _on_mouse_exited() -> void:
	if not selected:
		highlighter.modulate = default_color_highlighter
		highlighter.visible = false


func _on_input_event(_viewport: Node, _event: InputEvent, _shape_idx: int) -> void:
	if Input.is_action_just_pressed("primary_action"):
		if Globals.first_selected and not selected:
			_second_selected()
		else:
			_first_selected()
		_update_warning()
		get_viewport().set_input_as_handled()


func _first_selected() -> void:
	selected = !selected
	highlighter.modulate = selected_color if selected else default_color_highlighter
	Globals.first_selected = self if selected else null


func _second_selected() -> void:
	var conn := Globals.first_selected.request_connection(self)
	if conn.ok:
		if conn.delete:
			for i in len(inbound_connections):
				if not inbound_connections[i] or inbound_connections[i].is_connected_to(Globals.first_selected):
					inbound_connections.remove_at(i)
					return
		inbound_connections.append(Globals.first_selected)
		Globals.first_selected = null
		_update_warning()
	selected = false


func _update_warning() -> void:
	var warning_text := ""
	if len(outbound_connections) < data.lines_out:
		warning_text += "Not enough outbound connections\n"
	elif len(outbound_connections) > data.lines_out:
		warning_text += "Too many outbound connections\n"
	
	if len(inbound_connections) < data.lines_in:
		warning_text += "Not enough inbound connections\n"
	elif len(inbound_connections) > data.lines_in:
		warning_text += "Too many inbound connections\n"
		
	if warning_text != "":
		if _warning:
			_warning.setup(warning_text)
		else:
			var new_warning: Warning = warning_scene.instantiate()
			new_warning.setup(warning_text)
			add_child(new_warning)
			new_warning.position.y -= 35
			_warning = new_warning
	else:
		if _warning:
			_warning.queue_free()
			_warning = null
