class_name ConnectorData extends Resource

@export_range(0, 3, 1) var lines_in
@export_range(0, 3, 1) var lines_out
