class_name Connection extends Node2D

@onready var visual: Line2D = $Line2D

var start: Connector
var end: Connector

func _ready() -> void:
	var grad := Gradient.new()
	grad.colors = [Color.RED, Color.GREEN]

## Setup the connection's start and end point
func setup(starting_point: Connector, end_point: Connector) -> void:
	start = starting_point
	end = end_point
	_try_update_visuals()

## Is one of the two endpoints of this connection the given connector?
func is_connected_to(s: Connector) -> bool:
	return start == s or end == s

func _try_update_visuals() -> void:
	visual.clear_points()
	if start and end:
		visual.add_point(to_local(start.global_position))
		visual.add_point(to_local(end.global_position))

func _on_line_2d_pressed() -> void:
	print("Line pressed")
