class_name Line2DHittable extends Line2D

const BUFFER = 12**2

signal on_mouse_down
signal on_mouse_up
signal pressed
signal toggled

@export var highlight_colour := Color.from_hsv(0,0,22)
@export var click_colour := Color.from_hsv(0,0,15)

var _default_colour: Color
var _highlighted := false
var _pressed := false:
	set(new):
		if _pressed != new:
			toggled.emit()
			if new:
				on_mouse_down.emit()
			else:
				on_mouse_up.emit()
				pressed.emit()
		_pressed = new

func _ready() -> void:
	_default_colour = default_color

func _process(_delta: float) -> void:
	# Copied and modified from https://www.reddit.com/r/godot/comments/f3dr76/how_to_make_clickable_line2d/
	# The global position where the click happened
	var mouse_click : Vector2 = get_global_mouse_position()
	
	# We calculate the squared distance, so we don't need to calculate the root for the distance of multiple vectors
	# If you want to increase the detection click range, you would do it here
	var squared_width : float = width*width
	
	# Iterate every section (points - 1 sections)
	for i in range(points.size()-1):
		# We get 2 points of the Line2D (essentialy a line) and get the closest point on that line to our mouse click
		# Don't forget to also translate the local points to the global position where the click is
		var closest_point : Vector2 = Geometry2D.get_closest_point_to_segment(mouse_click, global_position + points[i], global_position + points[i+1])
		# If the distance of the closest point and the mouse click is smaller or equal the mouse position, it was clicked within the line
		# Also accounts for the circle of connectors
		if closest_point.distance_squared_to(mouse_click) <= squared_width and \
		get_distance_to_connector(mouse_click, (get_parent() as Connection).start) > BUFFER and \
		get_distance_to_connector(mouse_click, (get_parent() as Connection).end) > BUFFER:
			# Mouse on line
			# Enable hover highlight
			if not _highlighted:
				_highlighted = true
				default_color = highlight_colour
			_pressed = Input.is_action_pressed("primary_action")
			break
		else:
			# Disable hover highlight
			if _highlighted:
				_highlighted = false
				default_color = _default_colour

## Assumes pos to be a global
func get_distance_to_connector(pos: Vector2, conn: Connector) -> float:
	return pos.distance_squared_to(conn.global_position)
