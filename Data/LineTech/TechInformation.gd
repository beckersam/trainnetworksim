class_name TechData extends Node

enum TechLevel {
	FullyManual, ## No tech support whatsoever
	Old, ## The old German system with some indicators on what to expect
	ECTS_1, ## Old but now digital and some utility tools
	ECTS_2, ## Half-automatic
	ECTS_3, ## Fully automatic
}

# TODO: Implement information for all of the tech levels
