class_name LineInformation extends Resource

@export_range(10, 500, 10) var max_speed ## Max speed on this line in km/h
@export var tech_level: TechData.TechLevel ## The level of tech this line has
@export_range(0.0, 1.0, 0.01) var deteriotation ## How much the line has deteriorated. 1.0 is fully broken, 0.0 perfectly healthy
@export var closed := false ## Is the line currently closed?
@export var high_speed_only := false ## Is the line for high speed trains only?
