# What
An attempt to create a simulator for train networks

# Goals (Not neccessarly in order, especially for later ones)
1. Creation of a network
2. Basic Pathfinding
3. Passenger simulation
4. More simulation (line conditions, speed settings)
5. Enforce custom lines
6. Calculate optimal routes

## Monetisation
Project will stay foss at least until the basic simulation works.

Afterwards I might choose to close it off in hopes of being able to sell the advanced simulation stuff to DB and others

Will still follow what I said about releasing source of closed source projects (so 1 year after version release the source code for that version follows)
