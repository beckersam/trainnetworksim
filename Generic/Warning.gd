class_name Warning extends Node2D

@onready var label: Label = $CanvasLayer/Label
@onready var canvas_layer: CanvasLayer = $CanvasLayer

var _text := "Placeholder warning"

func setup(text: String) -> void:
	_text = text


func _process(_delta: float) -> void:
	if not canvas_layer.visible:
		return
	label.global_position = get_global_mouse_position()
	label.position.y -= label.get_line_height() * label.get_line_count()


func _on_mouse_entered() -> void:
	canvas_layer.visible = true
	label.text = _text


func _on_mouse_exited() -> void:
	canvas_layer.visible = false
